## PEM_freq_search.ipynb

This code takes the Amplitude Spectral Density (ASD) at set frequencies of every PEM channel as a function of time. This can be useful for tracking the appearance and change of peaks in the PEM ASDs over time. It takes in a gwpy timeseries for each channel and saves it to a pandas DataFrame and a .txt file in the same directory as the code for it can get hungup from time to time.
